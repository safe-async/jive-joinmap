use std::time::Duration;

struct Guard;

impl Drop for Guard {
    fn drop(&mut self) {
        println!("Dropped");
    }
}

fn main() {
    jive::block_on(async move {
        let mut set = jive_joinmap::JoinSet::new();

        for _ in 0..20 {
            while set.len() >= 10 {
                set.join_next().await;
                println!("Joined");
            }

            set.spawn(async move {
                let guard = Guard;
                jive::time::sleep(Duration::from_secs(2)).await;
                println!("Slept");
                drop(guard);
            });
            set.spawn_local(async move {
                let guard = Guard;
                jive::time::sleep(Duration::from_secs(2)).await;
                println!("Slept");
                drop(guard);
            });
            println!("Spawned");
            jive::time::sleep(Duration::from_millis(250)).await;
        }

        drop(set);
        println!("Dropped set");
        jive::time::sleep(Duration::from_millis(250)).await;
    })
}
