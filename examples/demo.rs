use std::time::Duration;

fn main() {
    jive::block_on(async move {
        let mut set = jive_joinmap::JoinSet::new();

        for _ in 0..20 {
            if set.len() >= 10 {
                set.join_next().await;
                println!("Joined");
            }

            set.spawn(async move {
                jive::time::sleep(Duration::from_secs(2)).await;
            });
            println!("Spawned");

            jive::time::sleep(Duration::from_millis(250)).await;
        }

        while set.join_next().await.is_some() {
            println!("Joined");
            // drain set
        }
    })
}
